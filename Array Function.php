<?php
$name=array('karim','rahim','Abdullah' );
$roll=array('12','10',10);
$nameandroll=array_combine($name,$roll);
print_r($nameandroll);

echo'</br>';
echo'</br>';
//-------------------------------------------------------------------------------------
function odd($var)
{
    // returns whether the input integer is odd
    return($var & 1);
}

function even($var)
{
    // returns whether the input integer is even
    return(!($var & 1));
}
//-------------------------------------------------------------------------------------
$array1 = array("a"=>1, "b"=>2, "c"=>3, "d"=>4, "e"=>5);
$array2 = array(6, 7, 8, 9, 10, 11, 12);

echo "Odd :\n";
print_r(array_filter($array1, "odd"));
echo'</br>';
echo "Even:\n";
print_r(array_filter($array2, "even"));

echo'</br>';
echo'</br>';
//-------------------------------------------------------------------------------------
$x = array("a" => 1, "b" => 1, "c" => 2);
$flipped = array_flip($x);

print_r($flipped);
echo'</br>';
echo'</br>';
//------------------------------------------------------------------------------------
$a=array("name"=>"Mosharaf","age"=>"30");
if (array_key_exists("name",$a))
{
    echo "Key exists!";
}
else
{
    echo "Key does not exist!";
}
echo'</br>';
echo'</br>';
//----------------------------------------------------------------------------------------
$a=array(10,20,30,"10");
print_r(array_keys($a,"10",false));
echo'</br>';
$a=array(10,20,30,"10");
print_r(array_keys($a,"10",true));
echo'</br>';
echo'</br>';
//--------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("red","green");
print_r(array_pad($a,5,"blue"));
echo'</br>';
echo'</br>';
$a=array("red","green");
print_r(array_pad($a,-5,"blue"));
//-----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a1=array("a"=>"red","b"=>"green");
$a2=array("c"=>"blue","b"=>"yellow");
print_r(array_merge($a1,$a2));
echo'</br>';
$a=array(3=>"red",4=>"green");
print_r(array_merge($a));
//-----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("red","green","blue");
array_pop($a);
print_r($a);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("red","green");
array_push($a,"blue","yellow");
print_r($a);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("red","green","blue","yellow","brown");
$random_keys=array_rand($a,3);
echo $a[$random_keys[0]]."<br>";
echo $a[$random_keys[1]]."<br>";
echo $a[$random_keys[2]];
echo'</br>';
echo'</br>';
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
print_r(array_rand($a,2));
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=['car','bus','truck'];
$b=['cat','dog'];
$c=array_replace($a,$b);
print_r($c);
echo'</br>';
echo'</br>';
$a=['car','bus','truck'];
$b=['cat','dog','tiger','lion'];
$c=array_replace($a,$b);
print_r($c);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a1=array("a"=>array("red"),"b"=>array("green","blue"),);
$a2=array("a"=>array("yellow"),"b"=>array("black"));

$result=array_replace_recursive($a1,$a2);
print_r($result);
echo'</br>';
echo'</br>';

$result=array_replace($a1,$a2);
print_r($result);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
$b=array_search('red',$a);
echo "the key of red is - $b";
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("Volvo","XC90",array("BMW","Toyota"));
$reverse=array_reverse($a);
$preserve=array_reverse($a,true);

print_r($a);
echo'</br>';
echo'</br>';
print_r($reverse);
echo'</br>';
echo'</br>';
print_r($preserve);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';

$b=['cat','dog','tiger','lion'];
$a=array_shift($b);
echo'</br>';
print_r($a);
echo'</br>';
print_r($b);
echo'</br>';
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=[110,120,115];
echo "The of Array is-".array_sum($a);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=[110,120,115,110,102];
print_r(array_unique($a));
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';

$b=['cat','dog','tiger','lion'];
print_r(array_unshift($b,'cow'));
echo'</br>';
echo'</br>';
print_r($b);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$a=array("a"=>"red","b"=>"green","c"=>"blue","d"=>"yellow");
print_r(array_values($a));

//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$age=array("Peter"=>"35","Ben"=>"37","Joe"=>"43");
echo "Peter is " . $age['Peter'] . " years old.";

echo'</br>';
echo'</br>';
$fruits=array("আম","জাম","কাঠাল");
$arrlength=count($fruits);

for($x=0;$x<$arrlength;$x++)
{
    echo $fruits[$x];
    echo "<br>";
}
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
asort($fruits);
foreach ($fruits as $key => $val) {
    echo "$key = $val";
    echo "<br>";
}

//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';

$firstname = "মোশারফ";
$lastname = "হোসাইন";
$age = "৩২";

$name = array("firstname", "lastname");
$result = compact($name, "age");

print_r($result);
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$fruits = array("d" => "lemon", "a" => "orange", "b" => "banana", "c" => "apple");
echo count($fruits);

//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$people = array("Peter", "Joe", "Glenn", "Cleveland");
echo '<pre>';
print_r (each($people));
echo '</pre>';
//----------------------------------------------------------------------------------------
echo'</br>';
echo'</br>';
$people = array("Peter", "Joe", "Glenn", "Cleveland");

echo current($people) . "<br>";
echo end($people);
