<?php
//If statement দ্বারা কোন কোড বা কোড ব্লক execute করা হয়
// যদি এর condition/expresion টি true হয়। যদি false হয়,
// তাহলে ব্লক এর কোড escape করে পরবর্তী কোড execute করবে।
$a=10;
$b=6;
if ($a > $b)
    echo "a is bigger than b";


echo '</br>';
echo '</br>';

//if-else if.....else দ্বারা কোড কন্ডিশন চেক করা হয়,
// যদি true হয় তাহলে কোডের প্রথমাংশ অর্থাৎ if এর অধীন
// কোডটি execute হবে। যদি false আসে তাহলে দিতীয় else
// এর অধীন কোডটি execute হবে। মনে রাখতে হবে condition হবে শুধুমাএ একটি।
$t = date("H");

if ($t < "20") {
    echo "Have a good day!";
} else {
    echo "Have a good night!";
}

echo '</br>';
echo '</br>';

$x=6;
switch ($x){
    case 1:
        echo"saturday";
        break;
    case 2:
        echo "Sunday";
        break;
    case 3:
        echo "Monday";
        break;
    case 4:
        echo "Tuesday";
        break;
    case 5:
        echo "Wednesday";
        break;
    case 6:
        echo "Thursday";
        break;
    case 7:
        echo "Friday";
}
echo '</br>';
// Continue statememt
echo '</br>';
$y=array(5,8,10,15);
$x=0;

while ($x<20){
    $x++;
    if (in_array($x,$y)){
        continue;
    }

    echo $x;


    echo '</br>';

}
echo '</br>';
echo '</br>';
for ($i=0;$i<=100;$i++){
    if ($i%2>0){
        echo $i;
    }
    echo '</br>';
}

echo '</br>';
$x=0;
while ($x<10){
    $x++;
    echo "2 X $x=".$x*2;
    echo '</br>';
}
echo '</br>';
echo '</br>';
$x=7;
while($x>0){
    $x--;
    echo str_repeat("*",$x);
    echo '</br>';

}

echo '</br>';
echo '</br>';
for ($i=1;$i<=20;$i++){
    if ($i%2==0){
        echo "even $i";
        echo '</br>';
    }else
        echo "odd $i";
    echo '</br>';

}
echo '</br>';

$i=0;
while ($i<4){
    $i++;
    echo 1111*$i.'</br>';
}

echo '</br>';

for ($i=1;$i<=4;$i++){
    echo 1111*$i.'</br>';
}